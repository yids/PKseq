#include <iostream>
#include <cstdlib>
#include "RtMidi.h"
#include <unistd.h>

struct sequencer
{
  RtMidiIn *sysexIn;                                                                                                                                                 
  RtMidiOut *sysexOut;
  RtMidiIn *clockIn;
  RtMidiOut *midiOut;

};


enum sysMsgType { NATIVE_START,
				  NATIVE_STOP,
			      SET_SYS_MIDI,
				  ALL_LEDS_ON,
				  ALL_LEDS_OFF,
				  LED_ON,
				  LED_OFF,
				  LED_BLINK,
				  LED_FLASH};

void printMsg(std::vector<unsigned char> message);
void sysexInCb( double deltatime, std::vector< unsigned char > *message, void * /*userData*/ );
void clockInCb(double deltatime, std::vector< unsigned char > *message, void *userData);
int sysexSend(sequencer *seq, sysMsgType msgType, int value);
void pk_init(sequencer *seq);

