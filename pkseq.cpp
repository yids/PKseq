#include <iostream>
#include <cstdlib>
#include "RtMidi.h"
#include <unistd.h>
#include "libpkseq.hpp"

int main()
{

  sequencer mySequencer;
  sequencer *seq = &mySequencer;

  pk_init(seq);

  std::cout << "\nPress enter to start native mode\n";
  char input;
  std::cin.get(input);



  sysexSend(seq, LED_FLASH, i);

  delete seq->sysexIn;
  delete seq->sysexOut;
  return 0;
}
