#include <iostream>                                                                                                                                                                  
#include <cstdlib>
#include "RtMidi.h"
#include <unistd.h>
#include "libpkseq.hpp"

namespace sysMsg
{
  unsigned char nativeStart[4] =  {0 ,0, 1,  247};
  unsigned char setSysMidi[45] =  {63, 42, 0, 0, 5, 9, 9, 16, 17, 127,
								   127, 3, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 1,
								   2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 247};
  unsigned char ledsOn[13]     = {63, 10, 1, 127, 127, 127, 127, 127, 3, 83, 69, 89, 247};
  unsigned char ledsOff[13]    = {63, 10, 1, 0, 0, 0, 0, 0, 3, 83, 69, 89, 247};
  unsigned char nativeStop[4]  = {0 ,0, 0,  247};
  unsigned char ledOn[4]       = {1, 0, 32, 247}; // element 1 is pad number
  unsigned char ledOff[4]      = {1, 0, 0,  247}; // element 1 is pad number 
  unsigned char ledFlash[4]    = {1, 0, 68, 247};  // element 1 is pad number 
  unsigned char ledBlink[4]    = {1, 0, 99, 247};
}


void printMsg(std::vector<unsigned char> message)
{
  std::cout<<"sysex msg: ";
  for (auto i: message)
    std::cout << int(i) << ' ';
  std::cout<<"\n";
}

void sysexInCb( double deltatime, std::vector< unsigned char > *message, void * /*userData*/ )
{
  unsigned int nBytes = message->size();
  for ( unsigned int i=0; i<nBytes; i++ )
    std::cout << "Byte " << i << " = " << (int)message->at(i) << ", ";
  if ( nBytes > 0 )
    std::cout << "stamp = " << deltatime << std::endl;
}

void clockInCb(double deltatime, std::vector< unsigned char > *message, void *userData)
{
  unsigned int nBytes = message->size();
  for ( unsigned int i=0; i<nBytes; i++ )
    std::cout << "Byte " << i << " = " << (int)message->at(i) << ", ";
  if ( nBytes > 0 )
    std::cout << "stamp = " << deltatime << std::endl;
}

int sysexSend(sequencer *seq, sysMsgType msgType, int value)
{
  unsigned char sysFirstBytes[5] = {240, 66, 64, 110, 8}; // first part of sysex msg
  std::vector<unsigned char> message(sysFirstBytes, sysFirstBytes+sizeof(sysFirstBytes));

  switch(msgType){
    case NATIVE_START:
	  std::cout<<"start native mode\n";
	  for(int i=0; i < int(sizeof(sysMsg::nativeStart)); i++)
		message.push_back(sysMsg::nativeStart[i]);
	  break;
	case NATIVE_STOP:
	  std::cout<<"stop native mode\n";
	  for(int i=0; i < int(sizeof(sysMsg::nativeStop)); i++)
		message.push_back(sysMsg::nativeStop[i]);
	  break;
    case SET_SYS_MIDI:
      for(int i=0; i < int(sizeof(sysMsg::setSysMidi)); i++)
        message.push_back(sysMsg::setSysMidi[i]);
	  break;
    case ALL_LEDS_ON:
      for(int i=0; i < int(sizeof(sysMsg::ledsOn)); i++)
        message.push_back(sysMsg::ledsOn[i]);
	  break;
	case ALL_LEDS_OFF:
      for(int i=0; i < int(sizeof(sysMsg::ledsOff)); i++)
        message.push_back(sysMsg::ledsOff[i]);
	  break;
	case LED_ON:
	  break;
	case LED_OFF:
	  break;
	case LED_BLINK:
	  for(int i=0; i < int(sizeof(sysMsg::ledBlink)); i++)
        message.push_back(sysMsg::ledBlink[i]);
        message[6]=value;
	  break;
	case LED_FLASH:
 	  std::cout<<"flashing \n";
	  for(int i=0; i < int(sizeof(sysMsg::ledFlash)); i++)
        message.push_back(sysMsg::ledFlash[i]);
        message[6]=value;
      break;
  }
#ifdef DEBUG_SYSEX
  printMsg(message);
#endif
  seq->sysexOut->sendMessage(&message); 
  return 0;
}

void pk_init(sequencer *seq)
{
  seq->sysexIn  = new RtMidiIn();
  seq->sysexOut = new RtMidiOut();
  seq->clockIn  = new RtMidiIn();
  seq->midiOut  = new RtMidiOut();
  seq->sysexIn->openVirtualPort( "PKseqSysexIn" );
  seq->sysexOut->openVirtualPort( "PKseqSysexOut" );
  seq->clockIn->openVirtualPort("ClockIn");
  seq->midiOut->openVirtualPort("MidiOut");

  seq->sysexIn->setCallback( &sysexInCb );
  seq->sysexIn->ignoreTypes( false, true, true );

  seq->clockIn->setCallback( &clockInCb);

  std::cout << "\nPress enter to start native mode\n";
  char input;
  std::cin.get(input);

  sysexSend(seq, NATIVE_START, 0);
  sysexSend(seq, SET_SYS_MIDI, 0);
  sysexSend(seq, ALL_LEDS_ON, 0);
  sysexSend(seq, ALL_LEDS_OFF, 0);
}



