CPP=g++
CFLAGS=-g  -Wall -D__LINUX_ALSA__ -lasound -lpthread -std=c++11
GTKFLAGS=`pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

all: pkseq

pkseq: pkseq.cpp
	$(CPP) -o pkseq pkseq.cpp libpkseq.cpp /home/yids/src/music/python-rtmidi-0.5b1/src/RtMidi.cpp $(CFLAGS)

clean: 
	rm pkseq
